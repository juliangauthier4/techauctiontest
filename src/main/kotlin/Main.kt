import Auction.AuctionConstants.DEFAULT_PRICE

fun main() {
    val auction = Auction()
    val buyers: MutableList<Buyer> = mutableListOf(
        Buyer("A", mutableListOf(110, 130)),
        Buyer("B", mutableListOf()),
        Buyer("C", mutableListOf(125)),
        Buyer("D", mutableListOf(105, 115, 90)),
        Buyer("E", mutableListOf(132, 135, 140)),
    )

    val bid: Bid? = auction.findWinningBid(buyers, DEFAULT_PRICE)
    bid?.let { println("The buyer ${bid.buyer} wins the auction at the price ${bid.amount} euros") }
        ?: run { println("No winner") }
}