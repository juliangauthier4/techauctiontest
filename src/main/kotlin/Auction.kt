import Auction.AuctionConstants.DEFAULT_PRICE

class Auction {

    object AuctionConstants {
        const val DEFAULT_PRICE = 100
        const val HIGH_PRICE = 150
    }

    /**
     * find the winner and the price of a second-price sealed-bid auction
     * return a winning Bid (just a buyer and a price) or null
     * hypothesis:
     *  - if no buyer or no bid above or equal the reserved price
     *  => no winner
     *  - in case of equality between 2 or more buyers
     *  => pick a random winner among them at the second-price
     *  (and the second price is the highest bid price from a non winning buyer...so it's the same)
     *  - if only one bid is above or equal to the reserved price, winning price is reserved price
     */
    fun findWinningBid(buyers: MutableList<Buyer>, reservedPrice: Int = DEFAULT_PRICE): Bid? {
        val correctBids = mutableListOf<Bid>()

        // filter bids and keep the best bid for each buyer
        buyers.forEach { buyer ->
            buyer.bids.filter { amount -> amount >= reservedPrice }
                .maxByOrNull { amount -> amount }
                ?.let { amount -> correctBids.add(Bid(buyer.name, amount)) }
        }

        // shuffle the resulting bids to have a random winner in case of equality because the sort is stable
        // it means that equal elements preserve their order relative to each other after sorting
        // then sort by descending amount the shuffled list
        correctBids.shuffle()
        correctBids.sortByDescending { it.amount }

        // find the name of the winner if it exists (in the first item)
        // find the second price if it exists (in the next item)
        // if not, the second price is the reserved price
        val buyerName = correctBids.firstOrNull()?.buyer
        val amount = correctBids.getOrNull(1)?.amount
        buyerName?.let {
            return if (amount!=null) Bid(buyerName, amount) else Bid(buyerName, reservedPrice)
        }
        // no winner
        return null
    }
}