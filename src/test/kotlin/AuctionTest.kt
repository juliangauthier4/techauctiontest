import Auction.AuctionConstants.DEFAULT_PRICE
import Auction.AuctionConstants.HIGH_PRICE
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.anyOf
import org.junit.Assert.*
import org.junit.Test

class AuctionTest {
    private val auction: Auction = Auction()

    @Test
    fun testFindWinningBid_Nominal() {
        val buyers: MutableList<Buyer> = mutableListOf(
            Buyer("A", mutableListOf(110, 130)),
            Buyer("B", mutableListOf()),
            Buyer("C", mutableListOf(125)),
            Buyer("D", mutableListOf(105, 115, 90)),
            Buyer("E", mutableListOf(132, 135, 140)),
        )

        assertEquals("E", auction.findWinningBid(buyers)?.buyer)
        assertEquals(130, auction.findWinningBid(buyers)?.amount)
    }

    @Test
    fun testFindWinningBid_NoWinner() {
        val buyers: MutableList<Buyer> = mutableListOf(
            Buyer("A", mutableListOf(110, 130)),
            Buyer("B", mutableListOf()),
            Buyer("C", mutableListOf(125)),
            Buyer("D", mutableListOf(105, 115, 90)),
            Buyer("E", mutableListOf(132, 135, 140)),
        )

        assertNull(auction.findWinningBid(buyers, HIGH_PRICE)?.buyer)
        assertNull(auction.findWinningBid(buyers, HIGH_PRICE)?.amount)
    }

    @Test
    fun testFindWinningBid_NoBid() {
        val buyers: MutableList<Buyer> = mutableListOf(
            Buyer("A", mutableListOf()),
            Buyer("B", mutableListOf()),
        )

        assertNull(auction.findWinningBid(buyers)?.buyer)
        assertNull(auction.findWinningBid(buyers)?.amount)
    }

    @Test
    fun testFindWinningBid_NoBuyer() {
        val buyers: MutableList<Buyer> = mutableListOf()

        assertNull(auction.findWinningBid(buyers)?.buyer)
        assertNull(auction.findWinningBid(buyers)?.amount)
    }

    @Test
    fun testFindWinningBid_OneBuyerOneBid() {
        val buyers: MutableList<Buyer> = mutableListOf(Buyer("C", mutableListOf(125)))

        assertEquals("C", auction.findWinningBid(buyers)?.buyer)
        assertEquals(DEFAULT_PRICE, auction.findWinningBid(buyers)?.amount)
    }

    @Test
    fun testFindWinningBid_OneBuyerOneLimitBid() {
        val buyers: MutableList<Buyer> = mutableListOf(Buyer("C", mutableListOf(DEFAULT_PRICE)))

        assertEquals("C", auction.findWinningBid(buyers)?.buyer)
        assertEquals(DEFAULT_PRICE, auction.findWinningBid(buyers)?.amount)
    }

    @Test
    fun testFindWinningBid_OneBuyerMultipleBids() {
        val buyers: MutableList<Buyer> = mutableListOf(
            Buyer("D", mutableListOf(105, 115, 90))
        )

        assertEquals("D", auction.findWinningBid(buyers)?.buyer)
        assertEquals(DEFAULT_PRICE, auction.findWinningBid(buyers)?.amount)
    }

    @Test
    fun testFindWinningBid_Equality() {
        val buyers: MutableList<Buyer> = mutableListOf(
            Buyer("A", mutableListOf(110, 140)),
            Buyer("B", mutableListOf()),
            Buyer("C", mutableListOf(125)),
            Buyer("D", mutableListOf(105, 115, 90)),
            Buyer("E", mutableListOf(132, 135, 140)),
        )

        assertThat(auction.findWinningBid(buyers)?.buyer, anyOf(`is`("A"), `is`("E")))
        assertEquals(140, auction.findWinningBid(buyers)?.amount)
    }

    @Test
    fun testFindWinningBid_MultipleEquality() {
        val buyers: MutableList<Buyer> = mutableListOf(
            Buyer("A", mutableListOf(110, 140)),
            Buyer("B", mutableListOf()),
            Buyer("C", mutableListOf(125)),
            Buyer("D", mutableListOf(140, 115, 90)),
            Buyer("E", mutableListOf(132, 135, 140)),
        )

        assertThat(auction.findWinningBid(buyers)?.buyer, anyOf(`is`("A"), `is`("E"), `is`("D")))
        assertEquals(140, auction.findWinningBid(buyers)?.amount)
    }

}
